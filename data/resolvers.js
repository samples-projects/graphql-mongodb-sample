import mongoose from "mongoose"
import {Friends} from "./DatabaseConnector"

const resolvers = {
    Query: {
         getFriend: (root, {id}) => {
             return new Promise((resolve, object) => {
                 Friends.findById(
                     {_id: id},
                     (error, friend) => {
                         if (error) reject(error);
                         else resolve(friend)
                     });
             });
         }
    }
    ,
    Mutation: {
        createFriend: (root, {input}) => {
            const newFriend = new Friends({
                firstName: input.firstName,
                lastName: input.lastName,
                gender: input.gender,
                age: input.age,
                language: input.language,
                email: input.email,
                contacts: input.contacts,
            });
            newFriend.id = newFriend._id;

            return new Promise((resolve, object) => {
                newFriend.save((error) => {
                    if (error) reject(error);
                    else resolve(newFriend)
                })
            });
        },
        updateFriend: (root, {input}) => {
            return new Promise((resolve, object) => {
                Friends.findOneAndUpdate(
                    {_id: input.id},
                    input,
                    {new: true},
                    (error, friend) => {
                        if (error) reject(error);
                        else resolve(friend)
                    });
            });
        }, deleteFriend: (root, {id}) => {
            return new Promise((resolve, object) => {
                Friends.remove(
                    {_id: id},
                    (error) => {
                        if (error) reject(error);
                        else resolve("Successfully deleted Friend")
                    });
            });
        },

    }
};


export default resolvers;
